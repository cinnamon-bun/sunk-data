#!/bin/sh

for NUM in 12_03 12_04 12_05 19_06 20_06 22_04; do

    echo === === $NUM === ===

    echo === DOWNLOAD ===
    [ ! -f "compressed/srtm_""$NUM"".zip" ] && wget -c http://srtm.geog.kcl.ac.uk/portal/srtm41/srtm_data_geotiff/srtm_$NUM.zip && mv srtm_$NUM.zip compressed/

    echo === UNCOMPRESS ===
    [ ! -f "uncompressed/srtm_""$NUM"".tif" ] && unzip -o compressed/srtm_$NUM.zip srtm_$NUM.tif && mv srtm_$NUM.tif uncompressed/

    # echo === RESIZE ===
    # gdal_translate -outsize 50% 50% -co compress=lzw SRTM_1km.tif SRTM_1km_10percent.tif

    # echo === COMPRESS ===
    # gdal_translate -outsize 100% 100% -co compress=lzw SRTM_1km.tif SRTM_1km_compressed.tif

    echo === SMOOTH ===
    if [ ! -f "uncompressed/srtm_""$NUM""_smooth.tif" ] ; then
        rm -f "uncompressed/srtm_""$NUM""_smooth.tif"
        gdalwarp -srcnodata -32768 -dstnodata -32768 -r average -ts 3000 0 -co compress=lzw uncompressed/srtm_$NUM.tif "uncompressed/srtm_""$NUM""_smooth_temp.tif"
        gdalwarp -srcnodata -32768 -dstnodata -32768 -r cubicspline -ts 12000 0 -co compress=lzw "uncompressed/srtm_""$NUM""_smooth_temp.tif" "uncompressed/srtm_""$NUM""_smooth.tif"
        rm -f "uncompressed/srtm_""$NUM""_smooth_temp.tif"
    fi

    echo === RAMP ===
    # [ ! -f uncompressed/srtm_$NUM_ramp.tif ]        && gdaldem color-relief -co compress=lzw uncompressed/srtm_$NUM.tif ramp.txt uncompressed/srtm_$NUM_ramp.tif
    [ ! -f "uncompressed/srtm_""$NUM""_smooth_ramp.tif" ] && gdaldem color-relief -co compress=lzw "uncompressed/srtm_""$NUM""_smooth.tif" ramp.txt "uncompressed/srtm_""$NUM""_smooth_ramp.tif"

    echo === MASK ===
    [ ! -f "uncompressed/srtm_""$NUM""_mask.tif" ]        && gdaldem color-relief -co compress=lzw "uncompressed/srtm_""$NUM"".tif" ramp_mask.txt "uncompressed/srtm_""$NUM""_mask.tif"

    echo === HILLSHADE ===
    # 25000 for world map
    [ ! -f "uncompressed/srtm_""$NUM""_hillshade.tif" ]        && gdaldem hillshade -s 50000 -co compress=lzw "uncompressed/srtm_""$NUM"".tif" "uncompressed/srtm_""$NUM""_hillshade.tif"
    # [ ! -f uncompressed/srtm_$NUM_smooth_hillshade.tif ] && gdaldem hillshade -s 50000 -co compress=lzw uncompressed/srtm_$NUM_smooth.tif uncompressed/srtm_$NUM_smooth_hillshade.tif

    echo
done

echo
echo =============== DONE ================
echo

