Code for generating global-warming related slippy tile maps.

DEPENDENCIES
--

- GDAL command line tools for processing GeoTIFFs
- TileMill
- Tilestache
- SRTM data index to view in Google Earth: http://www.ambiotek.com/srtm
- This is only tested on Mac.  It should also work on Linux but now Windows.

GENERATING ELEVATION GEOTIFFS
--

This script:

    /srtm/process.sh

is sort of a makefile which downloads and
processes NASA elevation data ("SRTM data").  This data comes in chunks
which are approximately Oregon-sized, and right now we're grabbing the
whole West Coast and pieces of the East Coast.  There's a Google Earth
file listed above which shows you the chunks and their code numbers, and
then you put those numbers at the top of the script.

It downloads whatever data it doesn't have yet and runs it through a series of
steps using GDAL command-line tools to smooth it, color-code by elevation, etc.

The output is a pile of GeoTIFFs of various kinds (hillshaded,
color-coded by elevation for low-lying areas, water vs land) for each
region.

You can also take any random image, like a screenshot of a map from some
PDF, and convert it to a GeoTIFF by making a list of some pixel
coordinates and their corresponding GPS coordinates, using this script:

    /srtm/to_warp/warp.sh


GENERATING MAP TILES
--

The GeoTIFFs go into TileMill where they're combined like Photoshop
layers and sliced up into tiles.  We're using this TileMill style file:

    /srtm/tilemill.css

The tiles are all rendered out and stored in a single big ".mbtiles"
file.  Rendering both coasts in full detail takes a few hours and
results in a few gigabytes of tiles.


SERVING TILES
--

Then run a normal webserver to serve a page which contains a Leaflet map:

    /server/index.html

A quick way to serve that index file is to run this python command in
the same directory:

    python -m SimpleHTTPServer

And also run a Tilestache server:

    /server/serve-tiles.sh

which serves the individual map tiles out of the combined .mbtiles file,
for the Leaflet map to grab from inside the browser.


WHAT NEEDS TO BE DONE
--

- I want to stop using TileMill because it's too manual, so I need
another way to slice GeoTIFFs into map tiles.  This can probably be done
using the GDAL tools but I haven't researched it yet.

- Get more map data (flooding, rain, etc).  Right now I just have
low-res screenshots from government PDF reports.  I can get these
onto the map but they look terrible.  I hope we can get the actual data
they used to make those maps.

- Make a prettier webpage containing the map.  Improve the Leaflet code
that handles the map layers.  Figure out how to serve different map
layers that can be toggled.
