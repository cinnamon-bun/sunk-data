#!/bin/sh

echo ===================================================================
rm -f tmp.tif
rm -f output.tif

echo ==== ADDING CONTROL POINTS AND CONVERTING TO GEOTIFF ====

gdal_translate \
    -gcp    66   44  -124.211796 41.998156 \
    -gcp  1382  122  -102.066422 40.996484 \
    -gcp  1323  787  -103.077164 32.008076 \
    -gcp   486  750  -117.124413 32.534223 \
    -gcp   967  427  -109.041023 36.985003 \
    -co compress=lzw \
    delta_precip_clean.png tmp.tif

echo
echo ==== WARPING ====

# mercator: EPSG:4326 (WGS84)
# gmaps: EPSG:3857
gdalwarp -order 1 -r near -s_srs EPSG:4326 -t_srs EPSG:4326 -co compress=lzw tmp.tif output.tif

rm -f tmp.tif

echo ===================================================================

